import 'package:flutter/material.dart';

import 'button_animation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: <Widget>[
          _sideBar(),
          Expanded(child: DownloadButtonAnimation()),
        ],
      ),
    );
  }

  _sideBar() => Container(
        width: 72,
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(vertical: 48),
        decoration: BoxDecoration(
          color: Color(0xFF404040),
          boxShadow: [
            BoxShadow(
              color: Color(0xFF7070707).withOpacity(0.15),
              spreadRadius: 4,
              blurRadius: 8,
              offset: Offset(2, 0),
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Icon(Icons.folder_open),
            SizedBox(height: MediaQuery.of(context).size.height * 0.2),
            Expanded(
              child: RotatedBox(
                quarterTurns: 3,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(child: _item("All", 0)),
                      Expanded(child: _item("Inspiration", 1)),
                      Expanded(child: _item("Favorite", 2)),
                      Expanded(child: _item("Popular", 3)),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  int currentIndex = 0;

  _item(String text, int index) => InkWell(
        onTap: () {
          setState(() {
            currentIndex = index;
          });
        },
        child: AnimatedContainer(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          duration: Duration(milliseconds: 300),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: (index == currentIndex)
                    ? Colors.orangeAccent
                    : Colors.transparent,
                width: 3,
              ),
            ),
          ),
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: (index == currentIndex) ? Colors.white : Colors.white54,
              ),
            ),
          ),
        ),
      );
}
