import 'package:flutter/material.dart';

class DownloadButtonAnimation extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<DownloadButtonAnimation>
    with TickerProviderStateMixin {
  Animation<double> loadingAnimation, tickAnimation;
  AnimationController loadingController, tickController;

  @override
  void initState() {
    loadingController = AnimationController(
      duration: Duration(milliseconds: 1200),
      vsync: this,
    );

    loadingAnimation =
        Tween<double>(begin: 0, end: 1).animate(loadingController)
          ..addListener(() {
            setState(() {});
          })
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              tickController.forward();
            }
          });

    tickController = AnimationController(
      duration: Duration(milliseconds: 1200),
      vsync: this,
    );

    final tickAnimationCurve =
        CurvedAnimation(parent: tickController, curve: Curves.elasticOut);

    tickAnimation = Tween<double>(begin: 0, end: 1).animate(tickAnimationCurve)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          // for reverse only after waiting for a second
          Future.delayed(Duration(seconds: 1), () {
            tickController.reverse();
            loadingController.reverse();
          });
        }
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          loadingController.forward();
        },
        child: Container(
          width: 200 * (1 + 48 / 200 - loadingAnimation.value),
          height: 48,
          decoration: BoxDecoration(
            color: Color(0xFF3CB371),
            borderRadius: BorderRadius.circular(24),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF7070707).withOpacity(0.15),
                spreadRadius: 4,
                blurRadius: 8,
                offset: Offset(2, 0),
              ),
            ],
          ),
          child: Center(
            child: loadingAnimation.value > 0
                ? loadingAnimation.value == 1 ? icon() : progressIndicator()
                : text(),
          ),
        ),
      ),
    );
  }

  Widget icon() => Container(
        child: Center(
          child: Icon(
            Icons.done,
            size: 24 * tickAnimation.value,
            color: Colors.white,
          ),
        ),
      );

  Widget progressIndicator() => Container(
        height: 20,
        width: 20,
        child: CircularProgressIndicator(
          strokeWidth: 3,
          value: loadingAnimation.value,
          backgroundColor: Colors.white30,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );

  Widget text() => Text(
        "Download",
        style: TextStyle(
          fontSize: 16,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      );
}
